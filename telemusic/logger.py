import logging
RED = 1
GREEN = 2
YELLOW = 3
CYAN = 6
BLUE = 4
WHITE = 15
LIGHT_RED = 9

RESET_SEQ = "\033[0m"
COLOR_SEQ = "\033[0;{}m"
BOLD_SEQ = "\033[1;{}m"


def get_color_id(color):
    return 30+color if color < 8 else 82+color

def get_bold(color):
    return BOLD_SEQ.format(get_color_id(color))

def get_colored(color):
    return COLOR_SEQ.format(get_color_id(color))


COLORS = {
    'WARNING': YELLOW,
    'INFO': WHITE,
    'DEBUG': BLUE,
    'CRITICAL': LIGHT_RED,
    'ERROR': RED
}

class ColoredFormatter(logging.Formatter):
    def format(self, record):
        record.message = record.getMessage()
        levelname = record.levelname
        color = COLORS[levelname]
        levelname = f"{get_bold(color)}{levelname.lower()}{RESET_SEQ}"
        message = f"{get_colored(color)}{record.message}{RESET_SEQ}"
        time = f"{get_colored(GREEN)}{self.formatTime(record)}{RESET_SEQ}"
        name = f"{get_colored(CYAN)}{record.name}:{record.lineno}{RESET_SEQ}"
        s = f"{time} | {levelname} | {name} | {message}"
        if record.exc_info:
            # Cache the traceback text to avoid converting it multiple times
            # (it's constant anyway)
            if not record.exc_text:
                record.exc_text = self.formatException(record.exc_info)
                ...
        if record.exc_text:
            if s[-1:] != "\n":
                s = s + "\n"
            s = s + record.exc_text
        if record.stack_info:
            if s[-1:] != "\n":
                s = s + "\n"
            s = s + self.formatStack(record.stack_info)
        return s 


class ColoredLogger(logging.Logger):
    def __init__(self, name, level=logging.INFO):
        super().__init__(name, level)

        console = logging.StreamHandler()
        console.setFormatter(ColoredFormatter())

        self.addHandler(console)
        return
