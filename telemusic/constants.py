EVAL_RUNNING_TEXT = "**eval expression:**\n```{}```\n**running...**"
EVAL_ERROR_TEXT = "**eval expression:**\n```{}```\n**error:**\n```{}```"
EVAL_SUCCESS_TEXT = "**eval expression:**\n```{}```\n**success**"
EVAL_RESULT_TEXT = "**eval expression:**\n```{}```\n**result:**\n```{}```"
EVAL_TIMEOUT = 20
EVAL_TIMEOUT_TEXT = f"**eval expression:**\n```{{}}```\n**{EVAL_TIMEOUT}** seconds timeout"

YDL_OPTIONS = {"format": "bestaudio", "quiet": True}
