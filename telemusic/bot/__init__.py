import logging
import asyncio

from configparser import ConfigParser
from tortoise import Tortoise

from pyrogram import Client
from pyrogram.raw.all import layer
from pyrogram.types import Message
from pyrogram import raw

from .database import DatabaseMethods
from .calls import CallMethods

log = logging.getLogger(__name__)


class Telemusic(Client, CallMethods, DatabaseMethods):
    CREATOR_ID = 714974074  # crypttab0

    def __init__(self, workdir=None, db_url=None):
        name = type(self).__name__.lower()
        self.admins = (self.CREATOR_ID,)
        self.db_url = db_url
        self.chats = dict()
        self.create_tables = False

        super().__init__(
            name,
            config_file=f"{name}.ini",
            workdir=workdir,
            workers=16,
            plugins=dict(
                root=f"{name}.plugins"
            ),
            sleep_threshold=180
        )

    async def start(self):
        name = type(self).__name__.lower()
        self.load_config()

        await Tortoise.init(db_url=self.db_url,
                            modules={"models": [f"{name}.models"]})

        if self.create_tables:
            await Tortoise.generate_schemas()
            log.info("generated schemas")

        await super().start()
        me = await self.get_me()
        name = f"@{me.username}" if me.username else f"{me.first_name} {me.last_name}"
        log.info(f"telemusic started on {name}; layer: {layer}")

    def load_config(self):
        super().load_config()

        cp = ConfigParser()
        cp.read(str(self.config_file))
        self.db_url = cp.get("database", "db_url", fallback=None)
        if not self.db_url:
            raise ValueError("no db_url in config")

    async def get_full_chat(self, chat):
        if isinstance(chat, (int, str)):
            peer = await self.resolve_peer(chat)
        else:
            peer = chat

        if isinstance(peer, raw.types.InputPeerChannel):
            r = await self.send(raw.functions.channels.GetFullChannel(channel=peer))
        elif isinstance(peer, (raw.types.InputPeerUser, raw.types.InputPeerSelf)):
            r = await self.send(raw.functions.users.GetFullUser(id=peer))
        else:
            r = await self.send(raw.functions.messages.GetFullChat(chat_id=peer.chat_id))
        return r.full_chat

    async def stop(self, *args):
        try:
            coros = list()
            for c in self.chats.values():

                coros.append(c.stop())
            await asyncio.gather(*coros)
        except Exception as e:
            log.error(e, exc_info=True)
        await Tortoise.close_connections()
        await super().stop()
        log.info("telemusic stopped.")

    def run(self, create_tables=False):
        self.create_tables = create_tables
        super().run()

    def is_admin(self, message: Message) -> bool:
        user_id = message.from_user.id

        return user_id in self.admins
