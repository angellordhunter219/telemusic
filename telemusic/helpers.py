import asyncio
import json
from functools import wraps, partial

from youtube_dl import YoutubeDL as YDL
from youtube_dl.utils import DownloadError
from pyrogram.raw.types import (UpdateGroupCall, UpdateGroupCallParticipants,
                                Message, DataJSON)

from .constants import YDL_OPTIONS


def connected_only(connect=False):
    def decorator(func):
        @wraps(func)
        async def wrapped(client, message: Message):
            if client.is_call_connected(message.chat.id):
                await func(client, message)
            else:
                if not connect:
                    await message.reply_text("not connected")
                else:
                    ok = await client.connect_voice(message.chat.id)
                    if ok:
                        await func(client, message)
                    else:
                        await message.reply_text("voice chat not started")
        return wrapped

    return decorator


def admins_only(func):
    @wraps(func)
    async def decorator(client, message: Message):
        if client.is_admin(message):
            await func(client, message)
        await message.delete()
    decorator.admin = True

    return decorator


async def process_request(req) -> (str, str, int):
    """
    returns title, audio url, duration
    """

    loop = asyncio.get_event_loop()
    with YDL(YDL_OPTIONS) as ydl:
        try:
            req = req if req.startswith("https://") else f"ytsearch:{req}"
            extract = partial(ydl.extract_info, req, download=False)
            if req.startswith("https://"):
                video = await loop.run_in_executor(None, extract)
            else:
                video = (await loop.run_in_executor(None, extract))['entries'][0]
        except DownloadError:
            return None
    # return video["title"], video["formats"][0]["url"], video["duration"]
    return video["title"], video["webpage_url"], video["duration"]


def to_data_json(d):
    return DataJSON(data=json.dumps(d))


def mention_user(user):
    return f"[{user.username}](tg://user?id={user.id})"


def format_song(song):
    return f"[{song.title}]({song.url})"


def format_seconds(secs):
    days, r = divmod(secs, 86400)
    hours, r = divmod(r, 3600)
    minutes, seconds = divmod(r, 60)

    days_string = f"{days} days, " if days else ""
    hours_string = f"{hours:0>2}:" if hours else ""
    minutes_string = f"{minutes:0>2}:"
    seconds_string = f"{seconds:0>2}"
    return days_string+hours_string+minutes_string+seconds_string 


def format_now_playing(song, req, place=0):
    if not song:
        return "nothing is playing now."
    else:
        return (f"now playing: {format_song(song)} `[{format_seconds(place)} / {format_seconds(song.duration)}]`\n\n"
                f"**Requested by**: {mention_user(req)}\n")


group_call_update_filter = lambda update: isinstance(update, (UpdateGroupCall, UpdateGroupCallParticipants))
