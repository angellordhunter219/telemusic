import logging
import json
import asyncio
from asyncio.subprocess import PIPE

from pyrogram.raw.functions.phone import GetGroupCall, LeaveGroupCall, JoinGroupCall
from tgcalls import GroupCallService

from .helpers import to_data_json, format_now_playing

log = logging.getLogger(__name__)


class CallMethods:
    async def connect_voice(self, chat_id):
        chat = await self.get_full_chat(chat_id)
        if not chat.call:
            return False
        # call = (await self.send(GetGroupCall(call=chat.call))).call
        call = chat.call
        gcall = GroupCallService(call.id, call.access_hash)
        gcall._call = call
        params = to_data_json(await gcall.join())

        # resp = (await self.join_group_call(call, params, False)).call
        # new_params = json.loads(resp.params.data)

        await gcall.connect({"transport": {"fingerprints": [], "candidates": [], "pwd": "", "ufrag": ""}})
        self.calls[chat_id] = gcall

        @gcall.on_start()
        async def _():
            await self.process_call(chat_id)

        @gcall.on_stop()
        async def _():
            await self.on_call_stop(chat_id)

        self.loop.create_task(gcall.start())
        return True

    async def process_call(self, chat_id):
        await self.clear_queue(chat_id)
        await self.send_message(chat_id, "connected to voice chat.")
        last_msg = None
        call = self.calls[chat_id]
        _call = call._call 
        while call.connected:
            if last_msg:
                await last_msg.delete()
            song = await self.get_song(chat_id)
            if not song:
                continue
            # await call.play(song.url)
            call.proc = await asyncio.create_subprocess_exec("mpv", song.url, "--no-video", "--audio-device=pulse/Pseudo",
                                                        stdout=PIPE, stderr=PIPE)
            last_msg = await self.send_message(chat_id, format_now_playing(song, await song.requested_by), disable_notification=True)
            await call.proc.communicate()
            await self.next_song(chat_id)
            await asyncio.sleep(0.1)

    async def skip(self, chat_id):
        call = self.calls.get(chat_id)
        await call.skip_track()
        try:
            call.proc.terminate()
        except ProcessLookupError:
            pass

    async def stop_call(self, chat_id):
        call = self.calls[chat_id]
        await call.stop()

    async def on_call_stop(self, chat_id):
        call = self.calls.pop(chat_id)
        _call = call._call
        call.proc.terminate()
        await self.clear_queue(chat_id)
        try:
            await self.leave_group_call(call=_call, source=call.ssrc)
        except Exception as e:
            log.error(e, exc_info=True)

    def is_call_connected(self, chat_id):
        call = self.calls.get(chat_id)
        return call.connected if call else None

    async def join_group_call(self, call, params, muted):
        return (await self.send(JoinGroupCall(call=call, params=params, muted=muted))).updates[0]

    async def leave_group_call(self, call, source):
        return (await self.send(LeaveGroupCall(call=call, source=source))).updates[0]

    async def count_call_participants(self, chat_id):
        call = self.calls.get(chat_id)
        return (await self.send(GetGroupCall(call=call._call))).call.participants_count
