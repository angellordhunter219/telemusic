import time
import asyncio
import logging
import json
from math import ceil

from loguru import logger
import pyrogram
from pyrogram.types import Message
from pyrogram.filters import command

from ..helpers import process_request, connected_only, mention_user, format_song, format_now_playing
from ..models import Song, User
from ..telemusic import Telemusic

log = logging.getLogger(__name__)


@Telemusic.on_message(command("join"))
async def join(client, message: Message):
    if client.is_call_connected(message.chat.id):
        await message.reply_text("already connected")
        return

    ok = await client.connect_voice(message.chat.id)
    if ok:
        await message.reply_text("connected.")
    else:
        await message.reply_text("voice chat not started")


@Telemusic.on_message(command("play"))
@connected_only(connect=True)
async def play(client, message: Message):
    req = " ".join(message.command[1:])
        
    video = await process_request(req)

    if not video:
        await message.reply_text("nothing found")
        return
    title, url, duration = video
    user, _ = await User.get_or_create(id=message.from_user.id)
    user.username = message.from_user.username or "user"
    await user.save()
    song = await Song.create(title=title, url=url, duration=duration,
                             requested_by=user)
    if not await client.is_queue_empty(message.chat.id):
        await message.reply_text(f"added {format_song(song)} to queue")
    await client.play(message.chat.id, song)


@Telemusic.on_message(command("skip"))
@connected_only()
async def skip(client, message: Message):
    chat_id = message.chat.id
    user_id = message.from_user.id
    if await client.is_queue_empty(chat_id):
        await message.reply_text("nothing is playing now")
    else:
        if await client.is_skipping(chat_id, user_id):
            await message.reply_text("you've already voted for skip")
            return
        skippers = await client.count_skippers(chat_id)
        call_participants = await client.count_call_participants(chat_id)
        need_votes = ceil(call_participants / 2)

        if skippers + 1 >= need_votes:
            await client.skip(chat_id)
            await client.clear_skip_vote(chat_id)
            await message.reply_text("song skipped")
        else:
            await client.add_skip_vote(chat_id, user_id)
            await message.reply_text(f"{skippers+1}/{need_votes} voted for skip.")


@Telemusic.on_message(command("stop"))
@connected_only()
async def stop(client, message: Message):
    await client.stop_call(message.chat.id)
    await message.reply_text("disconnected")


@Telemusic.on_message(command(["now_playing", "np"]))
@connected_only()
async def now_playing(client, message: Message):
    song, place = await client.get_current_song(message.chat.id)
    if song:
        await message.reply_text(format_now_playing(song, await song.requested_by, place), disable_notification=True)
    else:
        await message.reply_text("nothing is playing now")


@Telemusic.on_message(command("queue"))
@connected_only()
async def queue(client, message: Message):
    if await client.is_queue_empty(message.chat.id):
        await message.reply_text("nothing is playing now")
        return

    songs = await client.get_queue(message.chat.id)

    song = songs[0]
    text = f"**now playing**: {format_song(song)}\n\n"

    if songs[1:]:
        text += "**queue**:\n"

        for i, song in enumerate(songs[1:], start=1):
            text += f"{i}. {format_song(song)}\n"
    else:
        text += "queue is empty."
    await message.reply_text(text, disable_web_page_preview=True)
