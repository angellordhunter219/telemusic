from random import Random
r = Random()


def random32() -> int:
    return r.getrandbits(28)

