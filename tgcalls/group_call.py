import logging
import asyncio

from inspect import isawaitable
from ipaddress import ip_address, IPv4Address, IPv6Address
# from aiortc import (RTCPeerConnection, RTCIceCandidate, RTCSessionDescription,
#                     RTCCertificate, RTCIceGatherer, RTCDtlsTransport, RTCDtlsFingerprint,
#                     RTCDtlsParameters, RTCIceParameters, RTCIceTransport,
#                     RTCIceServer)
# from aiortc.contrib.signaling import BYE, TcpSocketSignaling
# from aiortc.contrib.media import MediaBlackhole, MediaPlayer, MediaRecorder
# from aiortc.rtcrtpparameters import (
#     RTCRtcpFeedback,
#     RTCRtpCodecParameters,
#     RTCRtpHeaderExtensionParameters,
# )
# from aiortc.sdp import GroupDescription, SessionDescription, SsrcDescription
from .helpers import random32

log = logging.getLogger(__name__)


class GroupCallService():
    def __init__(self, id, access_hash, on_close=None):
        self.id = id
        self.access_hash = access_hash
        self.ssrc = random32()
        self._on_start = None
        self._on_stop = None
        self.loop = asyncio.get_event_loop()

    async def stop(self, *args):
        log.info(f"stop group call id: {self.id}; hash: {self.access_hash}")

    async def join(self):
        log.info(f"joining group call id: {self.id}; hash: {self.access_hash}")
        params = {"ssrc": self.ssrc}

        return params

    def on_start(self):
        def decorator(func):
            if self._on_start:
                raise RuntimeError("hook on_start already set")
            self._on_start = func
            return func
        return decorator

    def on_stop(self):
        def decorator(func):
            if self._on_stop is not None:
                raise RuntimeError("hook on_stop already set")
            self._on_stop = func
            return func
        return decorator

    async def connect(self, params):
        params = params["transport"]
        fingerprints = params["fingerprints"]
        candidates = params["candidates"]
        pwd = params["pwd"]
        ufrag = params["ufrag"]

        ipv4_endpoints = list()
        ipv6_endpoints = list()
        # self.pc = RTCPeerConnection()

        log.debug(f"candidates: {candidates}")
        log.debug(f"fingerprints: {fingerprints}")
        log.debug(f"pwd: {pwd}")
        log.debug(f"ufrag: {ufrag}")
        urls = list()
        _fingerprints = list()

        # for c in candidates:
            # ice_candidate = RTCIceCandidate(c["component"], c["foundation"],
            #                                 c["ip"], c["port"], c["priority"],
            #                                 c["protocol"], c["type"],
            #                                 sdpMid=c["id"])
            # await self.pc.addIceCandidate(ice_candidate)
            # url = f"stun:['ip']:c['port']"
            # urls.append(url)

        # ice_server = RTCIceServer(urls, ufrag, pwd)
        
        # for f in fingerprints:
        #     _fingerprints.append(RTCDtlsFingerprint(f['hash'], f["fingerprint"]))
        
        # ice_transport = RTCIceTransport(RTCIceGatherer([ice_server]))
        # ice_params = RTCIceParameters(ufrag, pwd)

        # dtls_transport = RTCDtlsTransport(ice_transport, [RTCCertificate.generateCertificate()])
        # dtls_params = RTCDtlsParameters(_fingerprints)

        # self.rtp = RTCRtpReceiver("audio", dtls_transport)
        self.connected = True
        # coro1 = dtls_transport.start(dtls_params)
        # coro2 = ice_transport.start(ice_params)
        # await coro1


    async def start(self):
        log.info(f"starting group call id: {self.id}; hash: {self.access_hash}")
        if self._on_start:
            await self._on_start()

        # @self.pc.on("datachannel")
        # def on_datachannel(channel):
            # start = time.time()
            # log.debug(f"pc received {channel}")

            # @channel.on("message")
            # async def on_message(message):
                # log.debug(f"channel received {message}")

    async def stop(self):
        self.connected = False
        # await self.pc.close()
        if self._on_stop:
            await self._on_stop()

    async def play(self, path):
        # self.pc.addTrack(MediaPlayer(path).audio)
        # await self.pc.setLocalDescription(await self.pc.createOffer())
        ...

    async def close(self): 
        self.connected = False
        if isawaitable(self.on_close):
            await self.on_close
        elif callable(self.on_close):
            self.on_close()
        await self.pc.close()

    async def skip_track(self):
        pass
